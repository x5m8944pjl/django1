from django.db import models

# Create your models here.
class Grade(models.Model):
    gname=models.CharField(max_length=20)
    gdate=models.DateField()
    ggrilnum=models.IntegerField()
    gboynum=models.IntegerField()
    isDelet=models.BooleanField(default=False)
    class Meta:
        db_table="grade"
        ordering=['id']
class StudentsManager(models.Manager):
    def get_queryset(self):
        return super(StudentsManager,self).get_queryset().filter(isDelet=False)
class Students(models.Model):

    stuobj=models.Manager()


    stuobj2=StudentsManager()
    sname = models.CharField(max_length=20)
    sdgender = models.BooleanField()
    sage = models.IntegerField()
    scontent = models.CharField(max_length=20)
    isDelet = models.BooleanField(default=False)
    sgrade=models.ForeignKey("Grade", on_delete=models.CASCADE)
    def __str__(self):
        return self.sname
    class Meta:
        db_table="students"
        ordering=['id']

    @classmethod
    def creatStudents(cls,name,gender,age,content,isD,grade):
        stu=cls(sname=name,sdgender=gender,sage=age,scontent=content,isDelet=isD,sgrade=grade)
        return stu


