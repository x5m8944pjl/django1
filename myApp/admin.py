from django.contrib import admin

# Register your models here.
from  .models import Grade,Students
class StudentsInfo(admin.TabularInline):
    model = Students
@admin.register(Grade)
class GradeAdmin(admin.ModelAdmin):
    inlines = [StudentsInfo]
    list_display=['pk','gname','gdate','gboynum','ggrilnum','isDelet']
    list_filter = ['gname']
    search_fields = ['gname']
    list_per_page = 5

    def __str__(self):
        return  self.gname
@admin.register(Students)
class StudentsAdmin(admin.ModelAdmin):
    def gender(self):
        if self.sdgender:
            return "male"
        else:
            return "female"
    list_display=['pk','sname','sage',gender,'scontent','isDelet']
    list_filter = ['sname']
    search_fields = ['sname']
    list_per_page = 10

    def __str__(self):
        return self.sname
    actions_on_bottom = True

#admin.site.register(Grade,GradeAdmin)
#admin.site.register(Students,StudentsAdmin)